var appsConfig = {
    "ACCESS_KEY": undefined,
    "APP_UNIQUE_ID": undefined,
    "DEPARTMENT_ID" : undefined
};
var initialAppsConfig = {
    "ACCESS_KEY": undefined,
    "APP_UNIQUE_ID": undefined,
    "DEPARTMENT_ID" : undefined
};
var curId = 1000;
var errorId = 1000;
var initTries = 0;
var initProcessId = 1;
var serverURL = "https://sms.ulgebra.com";

function fetchEventsAndShow() {
    var reqObj = {
        url: `https://calendly.com/api/v1/users/me/event_types?include=owner`,
        type: "GET",
        headers: {
            "X-TOKEN": (appsConfig.ACCESS_KEY === null ? initialAppsConfig.ACCESS_KEY : appsConfig.ACCESS_KEY)
        },
        postBody: {}
    };
    var applicationProcess = curId++;
    showProcess(`Fetching Calendly Events ...`, applicationProcess);
    $("#result-holder").html('<div class="event-fetch-status">Loading...</div>');
    ZOHODESK.request(reqObj).then(function (response) {
        $("#result-holder").html("");
        processCompleted(applicationProcess);
        var responseJSON = JSON.parse(response);
        if (responseJSON["statusCode"] === 200 || responseJSON["statusCode"] === 201 || responseJSON["statusCode"] === 204 || responseJSON["statusCode"] === 404) {
            var data = JSON.parse(responseJSON["response"]);
            if (!(data instanceof Object)) {
                data = JSON.parse(data);
            }
            if(data.count === 0){
                $("#result-holder").html('<div class="event-fetch-status">No Events Available</div>');
            }
            else{
              for(var i in data.data){
                  var item = data.data[i];
                  if(!item.attributes.active){
                        continue;;
                  }
                  $("#result-holder").append(`<div id="${item.id}" class="event-item" style="border-color: ${item.attributes.color}">
                <div class="eventName">
                    ${item.attributes.name}
                </div>
                <div class="eventSubType">
                    <div class="eventDuration">
                       <i class="material-icons">access_time</i> ${getHourDisplay(item.attributes.duration)}
                    </div>
                    <div class="eventType">
                        ${item.relationships.owner.data.type}
                    </div>
                </div>
                <div class="eventLink">
                    <input type="text" id="inp-eventLink-${item.id}" class="input-form" readonly value="${item.attributes.url.replace("https://calendly.com", "")}"/>
                    <button class="btn-copy" onclick="copyEventLink('${item.id}','${item.attributes.url}')">Copy Link</button>
                </div>
            </div>`);
              }  
            }
        }
        else if(responseJSON["statusCode"] === 401){
                     showInvalidCredsError();
                }
                else{
                    showTryAgainError();
                }
    }).catch(function (err) {
        showTryAgainError();
        processCompleted(applicationProcess);
        console.log(err);
    });
}

function copyEventLink(eventId, eventLink){
    var copyText = document.getElementById("inp-eventLink-"+eventId);
    copyText.value = eventLink;
    copyText.select();
    copyText.setSelectionRange(0, 99999)
    document.execCommand("copy");
    copyText.value = eventLink.replace("https://calendly.com", "");
    $("#"+eventId+" .btn-copy").text("Copied");
}

function initializeFromConfigParams(){
    initTries++;
    if(initTries > 3){
        showErroMessage('Kindly refresh the page to start the configuration.');
        return;
    }
    var applicationProcess = curId++;
    if(initTries === 1){
        showProcess(`Initializing app ...`, initProcessId);
    }
        showProcess(`Fetching application configuration ...`, applicationProcess);
        ZOHODESK.get("extension.config").then(function (response) {
            processCompleted(applicationProcess);
            var data = response['extension.config'];
            for (var item in data) {
                var configname = data[item]['name'];
                var configValue = data[item]['value'];
                if(!valueExists(configValue)){
                    continue;
                }
                if (configname === 'ACCESS_KEY') {
                    appsConfig.ACCESS_KEY = configValue;
                    initialAppsConfig.ACCESS_KEY = configValue;
                }
                if (configname === 'DEPARTMENT_ID') {
                    appsConfig.DEPARTMENT_ID = configValue;
                    initialAppsConfig.DEPARTMENT_ID = configValue;
                }
                if (configname === 'APP_UNIQUE_ID') {
                    appsConfig.APP_UNIQUE_ID = configValue;
                    initialAppsConfig.APP_UNIQUE_ID = configValue;
                }
            }
//            showHelpWinow(false);
            if(valueExists(appsConfig.APP_UNIQUE_ID)){
                processCompleted(initProcessId);
            }
            else{
                setTimeout(initializeFromConfigParams, 5000);
            }
            fetchEventsAndShow();
        }).catch(function (err) {
            showTryAgainError();
            processCompleted(applicationProcess);
            console.log(err);
        });

}
window.onload = function () {
    ZOHODESK.extension.onload().then(function (App) {
        
        initializeFromConfigParams();

        ZOHODESK.get("portal.id").then(function (response) {
            appsConfig.UA_DESK_ORG_ID = response['portal.id'];
        }).catch(function (err) {
            console.log(err);
        });

    });
};

function showProcess(text, id){
    $(".process-window-outer").show();
    $("#process-window-items").append(`<div id="process-item-${id}" class="process-window-item">${text}</div>`);
}

function showErroMessage(html){
    showErroWindow('Unable to process your request', html);
}
function showErroWindow(title, html){
    var id = errorId++;
   $('body').append(`<div class="error-window-outer" id="error-window-${id}">
            <div class="error-window-inner">
                <div class="error-window-title">
                    ${title}
                </div>
                <div class="error-window-detail">
                    ${html}
                </div>
                <div class="error-window-close" onclick="removeElem('#error-window-${id}')">
                   <i class="material-icons">close</i> Close
                </div>
            </div>
        </div>`);
}
function removeElem(sel){
    $(sel).remove();
}
function hideElem(sel){
    $(sel).hide();
}
function showHelpItem(helpId){
    $('#helpWindow').show();
    $('.help-window-item').hide();
    $("#"+helpId).show();
}
function processCompleted(id){
    $(`#process-item-${id}`).remove();
    if(($("#process-window-items").children().length) === 0){
        $(".process-window-outer").hide();
    }
}
function closeAllErrorWindows(){
    $('.error-window-outer').remove();
}
function showTryAgainError(){
    showErroMessage('Try again later');
}
function showInvalidCredsError(){
    showErroMessage('Given Calendly API Key is Invalid <br><br> Try again with proper Calendly API Key from <a href="https://calendly.com/integrations" title="Click to go to Calendly dashboard" target="_blank" noopener nofollow>Calendly dashboard</a>.');
}
function valueExists(val) {
    if(val!=null){
        val+="";
    }
    return val !== null && val !== undefined && val.length > 1 && val!=="null";
}
function getHourDisplay(totalMinutes){
    var hours = Math.floor(totalMinutes / 60);          
    var minutes = totalMinutes % 60;
    if(hours === 0){
        return minutes+"mins";
    }else{
        return hours +":"+(minutes < 10 ? "0"+minutes: minutes);
    }
}