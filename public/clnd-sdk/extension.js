var appsConfig = {
    "ACCESS_KEY": undefined,
    "APP_UNIQUE_ID": undefined,
    "DEPARTMENT_ID" : undefined
};
var initialAppsConfig = {
    "ACCESS_KEY": undefined,
    "APP_UNIQUE_ID": undefined,
    "DEPARTMENT_ID" : undefined
};
var curId = 1000;
var errorId = 1000;
var initTries = 0;
var initProcessId = 1;
var serverURL = "https://sms.ulgebra.com";
function syncInputValues() {

}
function resolveCurrentProcessView() {
    $('#input-api-key').val(appsConfig.ACCESS_KEY);
    if (valueExists(appsConfig.ACCESS_KEY)) {
        $('#input-api-key').attr({'readonly':true}).val(appsConfig.ACCESS_KEY.substr(0,5)+"xxxxxxxxxxxxx");
        $('#dept-chooser').hide();
        $("#dept-value").text(appsConfig.DEPARTMENT_ID.substring (appsConfig.DEPARTMENT_ID.indexOf("-")+1)).css({'margin-top':'5px', 'font-size':'18px', 'font-weight':'bold'}).show();
        $(".btn-save").hide();
        fetchWebhookInfoAndExecute((function(hookId){
            if(hookId === null){
                $("#incoming-integ-status").removeClass('c-orange').removeClass('c-green').removeClass('c-crimson').addClass('c-silver').html('<i class="material-icons">error</i> Reset configurate and save Access Key ');
            }else{
                $("#incoming-integ-status").removeClass('c-crimson').removeClass('c-orange').removeClass('c-silver').addClass('c-green').html('<i class="material-icons">check_circle</i> Calendly - Integration enabled');
            }
        }));
    }
    else {
        $(".btn-save").show();
        $("#dept-value").hide();
        $('#dept-chooser').show();
        $('#dept-select-label').text('Select a department');
        $('#input-api-key').removeAttr('readonly');
        $("#incoming-integ-status").removeClass('c-silver').removeClass('c-orange').removeClass('c-green').addClass('c-crimson').html('<i class="material-icons">error</i> Provide Access Key to enable integration');
    }
}
function showHelpWinow(manual){
    if(!valueExists(appsConfig.ACCESS_KEY)){
        showHelpItem('step-1');
    }
    else{
        if(manual){
            showHelpItem('step-youtube');
        }
    }
}
function resetPhoneNumerVal() {
    $('#phone-select-options').html("");
    $('#dept-select-label').text('Select a department');
    $('#dept-select-label').attr({'data-selectedval': 'null'});
     appsConfig.DEPARTMENT_ID = undefined;
}
function selectDropDownItem(elemId, val, displayVal) {
    $('.dropdown-holder').hide();
    $('#' + elemId).text(displayVal);
    $('#' + elemId).attr({'data-selectedval': val});
    appsConfig.DEPARTMENT_ID = val;
}

function showDropDown(elemId) {
    elemId = "phone-select-options";
    if($('#' + elemId).is(":visible")){
        $('#' + elemId).hide();
    }
    else{
        $('#' + elemId).show();
        if(elemId === 'phone-select-options'){
            fetchDepartmentsAndShow();
        }
    }
    
}
function fetchDepartmentsAndShow() {
    var reqObj = {
        url: `https://desk.zoho.com/api/v1/departments`,
        type: "GET",
        connectionLinkName: "readreceiptconnection",
			headers:{
				"orgId" : appsConfig.UA_DESK_ORG_ID
			},
        postBody: {}
    };
    $('#phone-select-options').html(`<div class="statusMsg">Loading...</div>`);
    ZOHODESK.request(reqObj).then(function (response) {
        var responseJSON = JSON.parse(response);
        if (responseJSON["statusCode"] === 200 || responseJSON["statusCode"] === 201) {
            var data = JSON.parse(responseJSON["response"]).statusMessage;
            console.log(data);
            $('#phone-select-options').html("");
            var deptArray = data.data;
                for (var i in deptArray) {
                    var obj = deptArray[i];
                        $('#phone-select-options').append(`<div class="dropdown-select-item" onclick="selectDropDownItem('dept-select-label','${obj.id+"-"+obj.name}', '${obj.name}')">${obj.name}</div>`);
                }
        }else if(responseJSON["statusCode"] === 401){
                     showInvalidCredsError();
                }
                else{
                    showErroWindow('Unable to departments', 'Try again later.');
                }
    }).catch(function (err) {
        showErroWindow('Unable to fetch Plivo mobile numbers', 'Try again later.');
        console.log(err);
    });
}
function valueExists(val) {
    if(val!=null){
        val+="";
    }
    return val !== null && val !== undefined && val.length > 1 && val!=="null";
}
function saveAPIKey() {
    
    var accessKey = $('#input-api-key').val();
    
    if(!valueExists(accessKey)){
        showErroWindow('API Key is missing','Kindly provide API Key, then proceed to save.');
        return;
    }
    
    if(!valueExists(appsConfig.DEPARTMENT_ID)){
        showErroWindow('Department not selected','Kindly select deparment to create event.');
        return;
    }
    
//    var authtokenChange = !valueExists(appsConfig.ACCESS_KEY);
//    if(authtokenChange){
        
        var reqObj = {
                url: `https://calendly.com/api/v1/users/me`,
                type: "GET",
                headers: {
                    "X-TOKEN": accessKey
                },
                postBody: {}
            };
            var phoneNumberProcess = curId++;
            showProcess(`Validating given configuration...`, phoneNumberProcess);
            createCustomFields();
            ZOHODESK.request(reqObj).then(function (response) {
                var responseJSON = JSON.parse(response);
                processCompleted(phoneNumberProcess);
                if (responseJSON["statusCode"] === 200) {
                    var authIdProcess = curId++;
                    showProcess('Saving Calendly Access Key ...', authIdProcess);
                    appsConfig.ACCESS_KEY = accessKey;
                    ZOHODESK.set('extension.config', {name: 'ACCESS_KEY', value: accessKey}).then(function (res) {
                        console.log(res);
                        processCompleted(authIdProcess);
                        createApplicationInMessageBird();
                        resolveCurrentProcessView();
                    }).catch(function (err) {
                        processCompleted(authIdProcess);
                        console.log(err);
                    });
                    var deptProcess = curId++;
                    showProcess('Setting departmentId ...', deptProcess);
                    ZOHODESK.set('extension.config', {name: 'DEPARTMENT_ID', value: appsConfig.DEPARTMENT_ID}).then(function (res) {
                        console.log(res);
                        processCompleted(deptProcess);
                        resolveCurrentProcessView();
                    }).catch(function (err) {
                        processCompleted(authIdProcess);
                        console.log(err);
                    });
                }
                else if(responseJSON["statusCode"] === 401){
                     showInvalidCredsError();
                }
                else{
                    showTryAgainError();
                }
            }).catch(function (err) {
                processCompleted(phoneNumberProcess);
                showTryAgainError();
                console.log(err);
            });
//        }
}
function showTryAgainError(){
    showErroMessage('Try again later');
}
function showInvalidCredsError(){
    showErroMessage('Given Calendly API Key is Invalid <br><br> Try again with proper Calendly API Key from <a href="https://calendly.com/integrations" title="Click to go to Calendly dashboard" target="_blank" noopener nofollow>Calendly dashboard</a>.');
}


function resetAllFields(){
    if(valueExists(appsConfig.ACCESS_KEY)){
    ZOHODESK.set('extension.config', {name: 'ACCESS_KEY', value: "null"}).then(function (res) {
        fetchWebhookInfoAndExecute(deleteApplicationInMessageBird);
       // appsConfig.ACCESS_KEY = null;
                        console.log(res);
                        resolveCurrentProcessView();
                    }).catch(function (err) {
                        showTryAgainError();
                        console.log(err);
                    });
                }
    ZOHODESK.set('extension.config', {name: 'DEPARTMENT_ID', value: "null"}).then(function (res) {
        fetchWebhookInfoAndExecute(deleteApplicationInMessageBird);
       // appsConfig.ACCESS_KEY = null;
                        console.log(res);
                        resolveCurrentProcessView();
                    }).catch(function (err) {
                        showTryAgainError();
                        console.log(err);
                    });
}

function fetchWebhookInfoAndExecute(callback){
    var reqObj = {
        url: `https://calendly.com/api/v1/hooks`,
        type: "GET",
        headers: {
            "X-TOKEN": (appsConfig.ACCESS_KEY === null ? initialAppsConfig.ACCESS_KEY : appsConfig.ACCESS_KEY)
        },
        postBody: {}
    };
    var applicationProcess = curId++;
    showProcess(`Checking desk application in Calendly ...`, applicationProcess);
    ZOHODESK.request(reqObj).then(function (response) {
        processCompleted(applicationProcess);
        var responseJSON = JSON.parse(response);
        if (responseJSON["statusCode"] === 200 || responseJSON["statusCode"] === 201 || responseJSON["statusCode"] === 204 || responseJSON["statusCode"] === 404) {
            var data = JSON.parse(responseJSON["response"]);
            if (!(data instanceof Object)) {
                data = JSON.parse(data);
            }
            if(data.count === 0){
                return callback(null);
            }
            else{
              for(var i in data.data){
                  var webhookJSON = data.data[i];
                  if(webhookJSON.attributes.url === getWebhookURL('create') || webhookJSON.attributes.url === getWebhookURL('cancel')){
                      callback(webhookJSON.id);
                  }
              }  
            }
        }
        else if(responseJSON["statusCode"] === 401){
                     showInvalidCredsError();
                }
                else{
                    showTryAgainError();
                }
    }).catch(function (err) {
        showTryAgainError();
        processCompleted(applicationProcess);
        console.log(err);
    });
}

function getWebhookURL(eventType){
    return `${serverURL}/public/callbacks/calendly/zohodesk?appOrgId=${appsConfig.UA_DESK_ORG_ID}&appSecurityContext=${appsConfig.APP_UNIQUE_ID}&deptId=${appsConfig.DEPARTMENT_ID.split('-')[0]}&e=${eventType}`;
}

function deleteApplicationInMessageBird(webhookId){
    if(!valueExists(webhookId)){
        return;
    }
    var reqObj = {
        url: `https://calendly.com/api/v1/hooks/${webhookId}/`,
        type: "DELETE",
        headers: {
            "X-TOKEN": (appsConfig.ACCESS_KEY === null ? initialAppsConfig.ACCESS_KEY : appsConfig.ACCESS_KEY)
        },
        postBody: {}
    };
    var applicationProcess = curId++;
    showProcess(`Removing existing desk application in Calendly ...`, applicationProcess);
    ZOHODESK.request(reqObj).then(function (response) {
        processCompleted(applicationProcess);
        var responseJSON = JSON.parse(response);
        if (responseJSON["statusCode"] === 200 || responseJSON["statusCode"] === 201 || responseJSON["statusCode"] === 204 || responseJSON["statusCode"] === 404) {
            resolveCurrentProcessView();
            window.location.reload();
        }
    }).catch(function (err) {
        showTryAgainError();
        processCompleted(applicationProcess);
        console.log(err);
    });
}

function closeAllErrorWindows(){
    $('.error-window-outer').remove();
}

function createApplicationInMessageBird(){
    var callbackURL = getWebhookURL('create');
    var reqObj = {
        url: `https://calendly.com/api/v1/hooks`,
        type: "POST",
        headers: {
            "X-TOKEN": appsConfig.ACCESS_KEY,
            "Content-Type": "application/x-www-form-urlencoded"
        },
        postBody: {
            "url" : callbackURL,
            "events[]": "invitee.created"
        }
    };
    var applicationProcess = curId++;
    showProcess(`Configuring desk application in Calendly ...`, applicationProcess);
    createCancelApplicationInMessageBird();
    ZOHODESK.request(reqObj).then(function (response) {
        processCompleted(applicationProcess);
        var responseJSON = JSON.parse(response);
        if (responseJSON["statusCode"] === 200 || responseJSON["statusCode"] === 201) {
            resolveCurrentProcessView(); 
        }
        else if (responseJSON["statusCode"] === 401){
            showInvalidCredsError();
        }
        else{
            showTryAgainError();
        }
    }).catch(function (err) {
        showTryAgainError();
        processCompleted(applicationProcess);
        console.log(err);
    });
}

function createCancelApplicationInMessageBird(){
    var callbackURL = getWebhookURL('cancel');
    var reqObj = {
        url: `https://calendly.com/api/v1/hooks`,
        type: "POST",
        headers: {
            "X-TOKEN": appsConfig.ACCESS_KEY,
            "Content-Type": "application/x-www-form-urlencoded"
        },
        postBody: {
            "url" : callbackURL,
            "events[]": "invitee.canceled"
        }
    };
    var applicationProcess = curId++;
    showProcess(`Configuring desk application in Calendly for Cancel Event...`, applicationProcess);
    ZOHODESK.request(reqObj).then(function (response) {
        processCompleted(applicationProcess);
        var responseJSON = JSON.parse(response);
        if (responseJSON["statusCode"] === 200 || responseJSON["statusCode"] === 201) {
            resolveCurrentProcessView(); 
        }
        else if (responseJSON["statusCode"] === 401){
            showInvalidCredsError();
        }
        else{
            showTryAgainError();
        }
    }).catch(function (err) {
        showTryAgainError();
        processCompleted(applicationProcess);
        console.log(err);
    });
}

function createCustomFields(){
                    {
                        var applicationProcess = curId++;
                        //showProcess(`Creating custom field Calendly-Event-ID ...`, applicationProcess);
                         var reqObj = {
                        url : `https://desk.zoho.com/api/v1/organizationFields?module=events`,
                        connectionLinkName : "readreceiptconnection",
                        type : "POST", 
                        headers:{
                            "orgId" : appsConfig.UA_DESK_ORG_ID
                        },
                        postBody:{
                            "displayLabel" : "Calendly-Event-ID",
                            "type" : "Text",
                            "maxLength": "15"
                          }
                        };
                        ZOHODESK.request(reqObj).then(function(response){
                             processCompleted(applicationProcess);
                            var responseJSON = JSON.parse(response);
                            if(responseJSON["statusCode"]===200 || responseJSON["statusCode"]===201){
                               
                            }else{
                                showTryAgainError();
                            }
                        }).catch(function(err){
                            processCompleted(applicationProcess);
                            showTryAgainError();
                            console.log(err);
                        });
                     }
                     
                     {
                         var applicationProcess = curId++;
                        //showProcess(`Creating custom field Cancel-Reason ...`, applicationProcess);
                         var reqObj = {
                        url : `https://desk.zoho.com/api/v1/organizationFields?module=events`,
                        connectionLinkName : "readreceiptconnection",
                        type : "POST", 
                        headers:{
                            "orgId" : appsConfig.UA_DESK_ORG_ID
                        },
                        postBody:{
                            "displayLabel" : "Cancel-Reason",
                            "type" : "Textarea",
                            "toolTip": "Cancel Reason"
                          }
                        };
                        ZOHODESK.request(reqObj).then(function(response){
                            processCompleted(applicationProcess);
                            var responseJSON = JSON.parse(response);
                            if(responseJSON["statusCode"]===200 || responseJSON["statusCode"]===201){
                                
                            }else{
                                showTryAgainError();
                            }
                        }).catch(function(err){
                            processCompleted(applicationProcess);
                            showTryAgainError();
                            console.log(err);
                        });
                     }
                }

function showProcess(text, id){
    $(".process-window-outer").show();
    $("#process-window-items").append(`<div id="process-item-${id}" class="process-window-item">${text}</div>`);
}

function showErroMessage(html){
    showErroWindow('Unable to process your request', html);
}
function showErroWindow(title, html){
    var id = errorId++;
   $('body').append(`<div class="error-window-outer" id="error-window-${id}">
            <div class="error-window-inner">
                <div class="error-window-title">
                    ${title}
                </div>
                <div class="error-window-detail">
                    ${html}
                </div>
                <div class="error-window-close" onclick="removeElem('#error-window-${id}')">
                   <i class="material-icons">close</i> Close
                </div>
            </div>
        </div>`);
}
function removeElem(sel){
    $(sel).remove();
}
function hideElem(sel){
    $(sel).hide();
}
function showHelpItem(helpId){
    $('#helpWindow').show();
    $('.help-window-item').hide();
    $("#"+helpId).show();
}
function processCompleted(id){
    $(`#process-item-${id}`).remove();
    if(($("#process-window-items").children().length) === 0){
        $(".process-window-outer").hide();
    }
}
function initializeFromConfigParams(){
    initTries++;
    if(initTries > 3){
        showErroMessage('Kindly refresh the page to start the configuration.');
        return;
    }
    var applicationProcess = curId++;
    if(initTries === 1){
        showProcess(`Initializing app ...`, initProcessId);
    }
        showProcess(`Fetching application configuration ...`, applicationProcess);
        ZOHODESK.get("extension.config").then(function (response) {
            processCompleted(applicationProcess);
            var data = response['extension.config'];
            for (var item in data) {
                var configname = data[item]['name'];
                var configValue = data[item]['value'];
                if(!valueExists(configValue)){
                    continue;
                }
                if (configname === 'ACCESS_KEY') {
                    appsConfig.ACCESS_KEY = configValue;
                    initialAppsConfig.ACCESS_KEY = configValue;
                }
                if (configname === 'DEPARTMENT_ID') {
                    appsConfig.DEPARTMENT_ID = configValue;
                    initialAppsConfig.DEPARTMENT_ID = configValue;
                }
                if (configname === 'APP_UNIQUE_ID') {
                    appsConfig.APP_UNIQUE_ID = configValue;
                    initialAppsConfig.APP_UNIQUE_ID = configValue;
                }
            }
            showHelpWinow(false);
            if(valueExists(appsConfig.APP_UNIQUE_ID)){
                processCompleted(initProcessId);
            }
            else{
                setTimeout(initializeFromConfigParams, 5000);
            }
            resolveCurrentProcessView();
        }).catch(function (err) {
            showTryAgainError();
            processCompleted(applicationProcess);
            console.log(err);
        });

}
window.onload = function () {
    ZOHODESK.extension.onload().then(function (App) {
        
        initializeFromConfigParams();

        ZOHODESK.get("portal.id").then(function (response) {
            appsConfig.UA_DESK_ORG_ID = response['portal.id'];
        }).catch(function (err) {
            console.log(err);
        });

    });
};